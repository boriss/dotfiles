require "PackerLoad"
require "Config"
require "LSP"

local cmd = vim.cmd

-- NERDCommenter
vim.g.NERDSpaceDelims = true
vim.g.NERDCompactSexyComs = true
vim.g.NERDDefaultAlign = 'left'
vim.g.NERDCommentEmptyLines = true
vim.g.NERDTrimTrailingWhitespace = true

-- Telescope
require('telescope').setup{
	defaults = {
        file_ignore_patterns = { '^vendor/', '^.git/' },
    },
	pickers = {
		buffers = {
			sort_mru = true,
			ignore_current_buffer = true
		},
		live_grep = {
			glob_pattern = "!vendor/*"
		},
		find_files = {
			hidden = true,
		}
	},
	extensions = {
		file_browser = {
		  -- theme = "ivy",
		  mappings = {
			["i"] = {
			  -- your custom insert mode mappings
			},
			["n"] = {
			  -- your custom normal mode mappings
			},
		  },
		},
	},
}
require("telescope").load_extension "file_browser"
vim.api.nvim_set_keymap('n', '<Leader>e', [[<cmd>Telescope find_files<CR>]], { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>g', [[<cmd>Telescope live_grep<CR>]], { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>n', [[<cmd>Telescope file_browser<CR>]], { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>b', [[<cmd>Telescope buffers<CR>]], { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>/', [[<cmd>Telescope current_buffer_fuzzy_find<CR>]], { noremap = true, silent = true })


-- Persist cursor position
cmd [[
  "" Remember cursor position
  augroup vimrc-remember-cursor-position
    autocmd!
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
  augroup END
]]

-- completion?
cmd [[
  let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']
]]
vim.g.completion_trigger_keyword_length = 3

-- Treesitter

require'nvim-treesitter.configs'.setup {
  ensure_installed = { 'c', 'cpp', 'go', 'lua', 'python', 'rust', 'typescript', 'vimdoc', 'cmake' },
  highlight = {
	enable = true
  },
  playground = {
	enable = true,
	disable = {},
	updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
	persist_queries = false -- Whether the query persists across vim sessions
  },
  textobjects = {
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        ["]m"] = "@function.outer",
        ["]]"] = "@function.outer",
      },
      goto_next_end = {
        ["]M"] = "@function.outer",
        ["]["] = "@function.outer",
      },
      goto_previous_start = {
        ["[m"] = "@function.outer",
        ["[["] = "@function.outer",
      },
      goto_previous_end = {
        ["[M"] = "@function.outer",
        ["[]"] = "@function.outer",
      },
    },
	select = {
      enable = true,
      -- Automatically jump forward to textobj, similar to targets.vim 
      lookahead = true,
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
      },
    },
    swap = {
      enable = true,
      swap_next = {
        ["<Leader>a"] = "@parameter.inner",
      },
      swap_previous = {
        ["<Leader>A"] = "@parameter.inner",
      },
    },
  },
}

require"fidget".setup{}

require"gitlinker".setup({
	  callbacks = {
        ["github.com"] = require"gitlinker.hosts".get_github_type_url,
        ["gitlab.com"] = require"gitlinker.hosts".get_gitlab_type_url,
        ["try.gitea.io"] = require"gitlinker.hosts".get_gitea_type_url,
        ["bitbucket.org"] = require"gitlinker.hosts".get_bitbucket_type_url,
        ["gitlab.bit9.local"] = require"gitlinker.hosts".get_gitlab_type_url
  },
})

-- split navigation
vim.api.nvim_set_keymap('n', '<C-k>', [[<C-w>k]], { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<C-j>', [[<C-w>j]], { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<C-h>', [[<C-w>h]], { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<C-l>', [[<C-w>l]], { noremap = true, silent = true })
