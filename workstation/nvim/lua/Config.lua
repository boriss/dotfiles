local cmd = vim.cmd
local o = vim.o
local g = vim.g
local api = vim.api
local has = vim.fn.has

if not has('gui_running') then
    o.t_Co = 256
end
o.background = 'dark'
if has('termguicolors') then
    cmd('let &t_8f = "\\<Esc>[38;2;%lu;%lu;%lum"')
    cmd('let &t_8b = "\\<Esc>[48;2;%lu;%lu;%lum"')
    o.termguicolors = true
end

-- systematic
api.nvim_set_keymap('n', ';', ':', { noremap = true, silent = true })
cmd [[ set guioptions=egmrti ]]
cmd [[ set guicursor=i:block ]]
vim.g.mapleader = "\\"
cmd [[ set timeoutlen=200 ]]
o.encoding = 'utf-8'
o.fileencoding = 'utf-8'
o.fileencodings = 'utf-8'
o.backup = false --
g.backup = false --
o.writebackup = false
g.writebackup = false
o.swapfile = false -- no .swap
g.swapfile = false -- no .swap
o.undofile = true -- use undo file
g.undofile = true -- use undo file
o.updatetime = 300 -- time (in ms) to write to swap file
o.ttyfast = true
o.ssop = ''
-- buffer
o.expandtab = false -- expand tab
o.tabstop = 4 -- tab stop
o.softtabstop = 4 -- soft tab stop
o.shiftwidth = 4 -- auto indent shift width
-- window
o.number = true
o.relativenumber = true
o.scrolloff = 10
-- editing
o.whichwrap = 'b,s,<,>,[,]' -- cursor is able to move from end of line to next line
o.backspace = 'indent,eol,start' -- backspace behaviors
-- search
o.ignorecase = true -- search with no ignore case
o.hlsearch = true -- highlight search
o.incsearch = true -- no incremental search
o.smartcase = true
o.completeopt = 'menuone,noselect' -- TODO:// Review this
o.hidden = true
o.ruler = true
o.colorcolumn = '80' -- display a color column when line is longer than 120 chars
o.mouse = 'a' -- enable mouse under normal and visual mode
g.mousehide = true
o.showmatch = true -- show bracket match
o.cmdheight = 2 -- height of :command line
o.wildmenu = true -- wildmenu, auto complete for commands
o.wildmode = 'list:longest,list:full'
o.splitright = true -- split to right
o.splitbelow = true -- split to below
o.shortmess = o.shortmess .. 'c'
o.clipboard = 'unnamed,unnamedplus'

cmd('colorscheme molokai')

