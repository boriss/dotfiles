local execute = vim.api.nvim_command
local install_path = vim.fn.stdpath('data')..'/site/pack/packer/opt/packer.nvim'

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  execute('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
end

vim.cmd [[packadd packer.nvim]]
vim.api.nvim_exec([[
  augroup Packer
    autocmd!
    autocmd BufWritePost plugins.lua PackerCompile
  augroup end
]], false)

local use = require('packer').use
require('packer').startup(function()
  use {'wbthomason/packer.nvim', opt = true}
  use {'kyazdani42/nvim-web-devicons'}

  use 'tpope/vim-fugitive'
  use 'preservim/nerdcommenter'

  use {'nvim-telescope/telescope.nvim', requires = {
		  'nvim-lua/popup.nvim',
		  'nvim-lua/plenary.nvim',
		  'nvim-telescope/telescope-symbols.nvim',
	  },
  }

  use 'nvim-telescope/telescope-file-browser.nvim'
  use 'neovim/nvim-lspconfig'
  use 'L3MON4D3/LuaSnip'
  use({
        'hrsh7th/nvim-cmp',
        requires = {
            'hrsh7th/cmp-buffer', -- buffer completions
            'hrsh7th/cmp-path', -- path completions
            'hrsh7th/cmp-cmdline', -- cmdline completions
            'saadparwaiz1/cmp_luasnip', -- snippet completions
            'hrsh7th/cmp-nvim-lsp',
            'hrsh7th/cmp-nvim-lua',
            -- Integrate with built-in lsp
            {
                'neovim/nvim-lspconfig',
                config = function()
                    require('user/configs/lsp')
                end,
            },
            -- Simple to use language server installer
            'williamboman/nvim-lsp-installer',
        },
  })

  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
  use 'nvim-treesitter/playground'
  use {'nvim-treesitter/nvim-treesitter-textobjects'}

  use 'tomasr/molokai'

  use {
    'NTBBloodbath/galaxyline.nvim',
      branch = 'main',
  }

  use 'nvim-lua/plenary.nvim'
  use {
    'ruifm/gitlinker.nvim',
    requires = 'nvim-lua/plenary.nvim',
  }

 use {
   'Bekaboo/dropbar.nvim',
   tag = 'v4.0.0',
 }

  use 'j-hui/fidget.nvim'
end)
