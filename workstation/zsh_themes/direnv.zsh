function direnv_prompt_info() {
    local ref
    ref=$(command direnv status)

    if [[ ! "$ref" == *"No .envrc"* ]];
    then
        echo "de:%{$fg_bold[green]%}√%{$reset_color%}"
    fi
}

function aws_profile_info() {
    local ref
    ref=$(tail -n2 ~/.aws/credentials)
	ref=${ref##"expiration = "}

	now=$(date '+%Y-%m-%d %H:%M:%S')

    if [[ $now < $ref ]];
    then
        echo "aws:%{$fg_bold[green]%}√%{$reset_color%}"
    fi
}
